# Project

## Getting started

Download the dataset:

```shell
make data
```

Run the analysis:

```shell
python explore_dataset.py
```
