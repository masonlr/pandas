#!/usr/bin/env python


import pandas as pd


NAMES = [
    "week",
    "deaths",
    "deaths_5_year_average",
    "influenza",
    "influenza_5_year_average",
    "covid",
]

df = pd.read_excel("data.xlsx", skiprows=4, nrows=16, names=NAMES)
df.to_csv("data.csv", index=False)
