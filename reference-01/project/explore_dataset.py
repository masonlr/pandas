import pandas as pd

# read the csv file
df = pd.read_csv("data.csv")

# plot a column
df.covid.plot()

# filter a column
df[df.week > 10].covid.plot()

# add a new column
df["ratio"] = df.covid / df.influenza
df.ratio.plot()
