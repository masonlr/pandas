import pandas as pd
import numpy as np

obj = pd.Series(range(3), index=["a", "b", "c"])
index = obj.index

index[1:]

# index is immutable
index[1] = "z"


labels = pd.Index(np.arange(3))
obj2 = pd.Series([1.5, -2.5, 0], index=labels)

obj2.index is labels

id(obj2.index)
id(labels)


pop = {"Nevada": {2001: 2.4, 2002: 2.9}, "Ohio": {2000: 1.5, 2001: 1.7, 2002: 3.6}}
frame3 = pd.DataFrame(pop)

# columns is also an index
frame3.columns

"Ohio" in frame3.columns
2003 in frame3.index


dup_labels = pd.Index(["foo", "foo", "bar", "bar"])

native_set = {"foo", "foo", "bar", "bar"}


a = pd.Index(["one", "two", "three"])
b = pd.Index(["three", "four", "five"])

a.difference(b)
a.union(b)
a.intersection(b)
a.isin(b)
a.is_unique
a.unique()


a.drop("one")
a.delete(0)
a.insert(0, "foo")
