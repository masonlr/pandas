# preference is data processing without `for` loops

# numpy: homogeneous array data
# pandas: heterogeneous table data

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# good to go through systematically from bottom up (away from your research problem)
# when you later look at stackoverflow, etc., you will understand the building blocks

# series, dataframe
obj = pd.Series([4, 7, -5, 3])

# extract a numpy array
obj.values
type(obj)
type(obj.values)
obj.index


obj2 = pd.Series([4, 7, -5, 3], index=["d", "b", "a", "c"])
obj2.index

# this is something unique to pandas
# (not available in numpy)
obj2["a"]
obj2["d"]
obj2[["c", "a", "d"]]


obj2[obj2 > 0]
obj2 * 2
np.exp(obj2)

# if this was a dict
obj_dict = {"d": 4, "b": 7, "a": -5, "c": 3}
"b" in obj_dict
"b" in obj2
"e" in obj2

# create a series from a dict
sdata = {"Ohio": 35000, "Texas": 71000, "Oregon": 16000, "Utah": 5000}
obj3 = pd.Series(sdata)
obj3

# change the index ordering
# the utah entry will come first
states = ["Utah", "Ohio", "Oregon", "Texas"]
custom_series = pd.Series(sdata, index=states)


# foreign index entry?
sdata = {"Ohio": 35000, "Texas": 71000, "Oregon": 16000, "Utah": 5000}
states = ["California", "Ohio", "Oregon", "Texas"]
obj4 = pd.Series(sdata, index=states)

pd.isnull(obj4)
pd.notnull(obj4)

# easier way (instance methods)
obj4.isnull()
obj4.notnull()

# only keep values that are in both
obj3
obj4
obj3 + obj4

# add names
obj4.name = "population"
obj4.index.name = "state"

# edit in place
obj
obj.index = ["Bob", "Steve", "Jeff", "Ryan"]
