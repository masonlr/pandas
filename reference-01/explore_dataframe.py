#!/usr/bin/env python


import pandas as pd
import numpy as np


# the main idea: pd.DataFrames are sets of columns. Each column is a ps.Series.
ohio = pd.Series([1.7, 3.6], index=[2001, 2002], name="Ohio")
nevada = pd.Series([2.4, 2.9], index=[2001, 2002], name="Nevada")
pdata = {"Ohio": ohio, "Nevada": nevada}
pd.DataFrame(pdata)


# dataframe: dict of series all sharing the same index

# this is only *one* of the ways to make a dataframe
# get lists of equal length and make a dict
|
data = {
    "state": ["Ohio", "Ohio", "Ohio", "Nevada", "Nevada", "Nevada"],
    "year": [2000, 2001, 2002, 2001, 2002, 2003],
    "pop": [1.5, 1.7, 3.6, 2.4, 2.9, 3.2],
}

frame = pd.DataFrame(data)

# the output looks different depending on whether you're using
# (i) a notebook or (ii) an ipython console or (iii) a python console, etc.

frame.head()
pd.DataFrame(data, columns=["year", "state", "pop"])


pd.DataFrame(data, columns=["year", "state", "pop", "debt"])

frame2 = pd.DataFrame(
    data,
    columns=["year", "state", "pop", "debt"],
    index=["one", "two", "three", "four", "five", "six"],
)

# select a column
frame2
frame2["state"]
frame2.state

# select a row
frame2.loc["three"]

frame2["debt"] = 16.5

frame2["debt"] = np.arange(6.0)


val = pd.Series([-1.2, -1.5, -1.7], index=["two", "four", "five"])

frame2["debt"] = val


frame2["eastern"] = frame2.state == "Ohio"

frame2.columns
del frame2["eastern"]
frame2.columns


# method 2
# outer keys: column names
# inner keys: row indices
pop = {"Nevada": {2001: 2.4, 2002: 2.9}, "Ohio": {2000: 1.5, 2001: 1.7, 2002: 3.6}}
frame3 = pd.DataFrame(pop)

# transpose
frame3.T


# set your own index (could be a subset or superset)
pd.DataFrame(pop, index=[2001, 2002])
frame3 = pd.DataFrame(pop, index=[2001, 2002, 2003])


ohio = pd.Series([1.7, 3.6], index=[2001, 2002], name="Ohio")
nevada = pd.Series([2.4, 2.9], index=[2001, 2002], name="Nevada")
pdata = {"Ohio": ohio, "Nevada": nevada}
df = pd.DataFrame(pdata)

df.index.name = "year"
df.columns.name = "state"
df.values


# object dtype for heterogeneous entries (some entries are numbers, others are nan or strings)
frame2.values


# Table 5.1 possible data inputs

# 2D ndarray
data = np.random.randint(low=1, high=10, size=(20, 3))
new_df = pd.DataFrame(data, columns=["a", "b", "c"])
new_df

# list of dicts
pop = [{"a": 2.4, "b": 2.9}, {"a": 1.4, "b": 5.9}]
pd.DataFrame(pop)


import numpy.ma as ma

data = np.array([[1, 2, 3, -1, 5], [12, 24, 32, -12, 45]])
masked_data = ma.masked_array(data, mask=[[0, 0, 0, 1, 0], [0, 0, 0, 1, 0]])
pd.DataFrame(masked_data)
