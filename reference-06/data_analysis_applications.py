#### Chapter 14 ####

## 1st application
## USA.gov data from bit.ly

# Load the data
import json

path = "example.txt"
records = [json.loads(line) for line in open(path)]

# Counting time zones in pure python
time_zones = [rec["tz"] for rec in records]

time_zones = [rec["tz"] for rec in records if "tz" in rec]
time_zones[:10]


def get_counts(sequence):
    counts = {}
    for x in sequence:
        if x in counts:
            counts[x] += 1
        else:
            counts[x] = 1
    return counts


counts = get_counts(time_zones)
counts["America/New_York"]
len(time_zones)


def top_counts(count_dict, n=10):
    value_key_pairs = [(count, tz) for tz, count in count_dict.items()]
    value_key_pairs.sort()
    return value_key_pairs[-n:]


top_counts(counts)

from collections import Counter

counts = Counter(time_zones)
counts.most_common(10)

# Counting time zones with pandas

import pandas as pd

frame = pd.DataFrame(records)
frame.info()
frame["tz"][:10]

tz_counts = frame["tz"].value_counts()
tz_counts[:10]

# Clean and visualize data
clean_tz = frame["tz"].fillna("Missing")
clean_tz[clean_tz == ""] = "Unknown"
tz_counts = clean_tz.value_counts()
tz_counts[:10]
# tz_counts.drop(index=["Unknown","Missing"])

import seaborn as sns

subset = tz_counts[:10]
sns.barplot(y=subset.index, x=subset.values)

# Agent string
frame["a"][50]

results = pd.Series([x.split()[0] for x in frame.a.dropna()])
results[:5]
results.value_counts()[:8]

# Decompose top time zones into Windows and non-Windows users.
cframe = frame[frame.a.notnull()]

import numpy as np

cframe["os"] = np.where(cframe["a"].str.contains("Windows"), "Windows", "Not Windows")
cframe["os"][:5]

by_tz_os = cframe.groupby(["tz", "os"])

agg_counts = by_tz_os.size().unstack().fillna(0)
agg_counts[:10]

# Use to sort in ascending order
indexer = agg_counts.sum(1).argsort()  # order of indexes that sort the index
indexer[:10]

count_subset = agg_counts.take(indexer[-10:])  # returns elements in given indexes
count_subset

agg_counts.sum(1).nlargest(10)
# df.iloc[::-1]

# Rearrange the data for plotting
count_subset = count_subset.stack()
count_subset.name = "total"
count_subset = count_subset.reset_index()
# count_subset[:10]
sns.barplot(x="total", y="tz", hue="os", data=count_subset)


def norm_total(group):
    group["normed_total"] = group.total / group.total.sum()
    return group


results = count_subset.groupby("tz").apply(norm_total)

sns.barplot(x="normed_total", y="tz", hue="os", data=results)

g = count_subset.groupby("tz")
results2 = count_subset.total / g.total.transform("sum")

### MovieLens 1M dataset

import pandas as pd

unames = ["user_id", "gender", "age", "occupation", "zip"]
users = pd.read_table(
    "users.dat", sep="::", header=None, names=unames
)

rnames = ["user_id", "movie_id", "rating", "timestamp"]
ratings = pd.read_table(
    "ratings.dat", sep="::", header=None, names=rnames
)

mnames = ["movie_id", "title", "genres"]
movies = pd.read_table(
    "movies.dat", sep="::", header=None, names=mnames
)

users[:5]
ratings[:5]
movies[:5]
ratings

data = pd.merge(pd.merge(ratings, users), movies)
data
data.iloc[0]

mean_ratings = data.pivot_table(
    "rating", index="title", columns="gender", aggfunc="mean"
)
mean_ratings[:5]

ratings_by_title = data.groupby("title").size()
ratings_by_title[:10]
active_titles = ratings_by_title.index[ratings_by_title >= 250]
active_titles

# Select rows on the index
mean_ratings = mean_ratings.loc[active_titles]
mean_ratings

top_female_ratings = mean_ratings.sort_values(by="F", ascending=False)
top_female_ratings[:10]

# Measuring rating disagreement

mean_ratings["diff"] = mean_ratings["M"] - mean_ratings["F"]

sorted_by_diff = mean_ratings.sort_values(by="diff")
sorted_by_diff[:10]

# Reverse order of rows, take first 10 rows
sorted_by_diff[::-1][:10]

# Standard deviation of rating grouped by title
rating_std_by_title = data.groupby("title")["rating"].std()
# Filter down to active_titles
rating_std_by_title = rating_std_by_title.loc[active_titles]
# Order Series by value in descending order
rating_std_by_title.sort_values(ascending=False)[:10]


# Important pandas functions
?pd.Series.value_counts
?pd.Series.str.contains
?pd.Series.argsort
?pd.DataFrame.unstack
?pd.DataFrame.stack
?pd.DataFrame.reset_index
?pd.DataFrame.take
?pd.DataFrame.apply
?pd.DataFrame.transform
?pd.DataFrame.sort_values
?pd.merge
?pd.pivot_table