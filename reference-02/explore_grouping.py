#!/usr/bin/env python

import pandas as pd
import numpy as np

# aiming to replace "pivot tables" from excel

df = pd.DataFrame(
    {
        "key1": ["a", "a", "b", "b", "a"],
        "key2": ["one", "two", "one", "two", "one"],
        "data1": np.random.randn(5),
        "data2": np.random.randn(5),
    }
)


df.data1
df.data1.groupby(df.key1)
df.data1.groupby(df.key1).mean()
df.data1.groupby([df.key1, df.key2]).mean()

# shorthand
df.groupby(["key1", "key2"]).mean()
df.groupby(df.key1).mean()


means = df.data1.groupby([df.key1, df.key2]).mean()
means.unstack()

# you can supply additional info
# make sure new data matches length for dataframe
states = np.array(["Ohio", "California", "California", "Ohio", "Ohio"])
years = np.array([2005, 2005, 2006, 2005, 2006])
df.data1.groupby([states, years]).mean()

# count the group sizes
df.groupby("key1").size()


# come up with an example dataset

# good examples of datasets
titanic = pd.read_csv(
    "https://raw.githubusercontent.com/mwaskom/seaborn-data/master/titanic.csv"
)

titanic.columns

titanic.groupby("sex").count()  # html
output = titanic.groupby("sex").count()
print(output)  # text

# https://pandas.pydata.org/pandas-docs/stable/user_guide/style.html
titanic.style.background_gradient(cmap="viridis")
