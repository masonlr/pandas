#!/usr/bin/env python

import pandas as pd
import numpy as np

obj = pd.Series([4.5, 7.2, -5.3, 3.6], index=["d", "b", "a", "c"])


# re-order the rows
obj2 = obj.reindex(["a", "b", "c", "d", "e"])


obj3 = pd.Series(["blue", "purple", "yellow"], index=[0, 2, 4])
obj3.reindex(range(6))
obj3.reindex(range(6), method="ffill")
obj3.reindex(range(6), fill_value="foo")


pd.DataFrame(np.arange(9).reshape(3, 3))
pd.DataFrame(np.arange(9).reshape(3, 3), index=["a", "c", "d"])
frame = pd.DataFrame(
    np.arange(9).reshape(3, 3),
    index=["a", "c", "d"],
    columns=["Ohio", "Texas", "California"],
)

# re-order the rows
frame.reindex(["a", "b", "c", "d"])

# re-order the columns
states = ["Texas", "Utah", "California"]
frame.reindex(columns=states)

# no longer works
frame.loc[["a", "b", "c", "d"], states]
