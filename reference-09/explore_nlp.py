import pandas as pd

# Original df
df_full = pd.read_csv("abstracts.csv")

df_full["author"] = df_full["contributor_0"]

df_full[df_full["author"] == "Assen Batchvarov"]

Mason_index = df_full.apply(
    lambda row: row.astype(str).str.contains("Lachlan Mason").any(), axis=1
)
df_full[Mason_index]
df_full["author"][df_full[Mason_index].index[0]] = "Lachlan Mason"


df_full[df_full["author"] == "Konstantinos Zinelis"]

df_full[df_full["author"] == "Gabriel Goncalves"]

df_full[df_full["author"] == "Indranil Pan"]


# df of authors and abstracts
df_base = df_full[["author", "abstract"]]

# Get rid of null
df_base.isnull().values.any()
df_base.isnull().sum()
df_base = df_base.dropna()

# Get rid of tbd
(df_base.abstract.str.len() <= 5).sum()
df_base[df_base.abstract.str.len() <= 5]
df_base = df_base[-(df_base.abstract.str.len() <= 5)]


# Get row
Pan_pos = df_base.loc[df_base["author"] == "Indranil Pan"]
Pan = df_base.iloc[Pan_pos.index[0]]

from nltk.stem import WordNetLemmatizer
from nltk.stem import PorterStemmer

lemmatizer = WordNetLemmatizer()

lemmatized = [lemmatizer.lemmatize(word) for word in Pan.abstract.split(" ")]
lemmatized[:30]

stemmer = PorterStemmer()

stemmer = [stemmer.stem(word) for word in Pan.abstract.split(" ")]
stemmer[:30]

# Apply a first round of text cleaning techniques
Batchvarov_pos = df_base.loc[df_base["author"] == "Assen Batchvarov"]
Batchvarov = df_base.iloc[Batchvarov_pos.index[0]]
Batchvarov.abstract

import re
import string


def clean_text(text):
    text = text.lower()
    text = re.sub("[%s]" % re.escape(string.punctuation), "", text)
    text = re.sub("\d*", "", text)
    text = re.sub("\n", "", text)
    return text


clean_text_fun = lambda x: clean_text(x)


df_clean1 = pd.DataFrame(df_base.abstract.apply(clean_text_fun))
df_clean1.index = df_base.author

df_clean1.iloc[Batchvarov_pos.index[0]].abstract


df_corpus = (
    df_clean1.groupby(["author"])["abstract"]
    .apply(lambda x: " ".join(x.astype(str)))
    .reset_index()
)
df_corpus.set_index("author", inplace=True)

# Create a document-term matrix using CountVectorizer, and exclude common English stop words
from sklearn.feature_extraction.text import CountVectorizer

cv = CountVectorizer(stop_words="english")
data_cv = cv.fit_transform(df_corpus.abstract)
df_dtm = pd.DataFrame(data_cv.toarray(), columns=cv.get_feature_names())
df_dtm.index = df_corpus.index
df_dtm


### EDA ###

## Word clouds
# Find the top 10 words said by each author

data_dtm = df_dtm.transpose()
data_dtm.head()


top_dict = {}
for c in data_dtm.columns:
    top = data_dtm[c].sort_values(ascending=False).head(10)
    top_dict[c] = list(zip(top.index, top.values))

top_dict

top_dict["Lachlan Mason"]

from wordcloud import WordCloud

wc = WordCloud(
    background_color="white", colormap="Dark2", max_font_size=150, random_state=42,
)

import matplotlib.pyplot as plt

plt.rcParams["figure.figsize"] = [16, 6]

pdgroup_names = [
    "Assen Batchvarov",
    "Lachlan Mason",
    "Konstantinos Zinelis",
    "Gabriel Goncalves",
    "Indranil Pan",
]
wc_dtm = data_dtm[pdgroup_names]
wc_corpus = df_corpus.loc[pdgroup_names]

# Create subplots for each author
for index, author in enumerate(wc_dtm.columns):
    wc.generate(wc_corpus.abstract[author])

    plt.subplot(2, 3, index + 1)
    plt.imshow(wc, interpolation="bilinear")
    plt.axis("off")
    plt.title(wc_dtm.columns[index])

plt.show()

## Word clouds with stop words
from collections import Counter

# Pull out the top 30 words for each author
words = []
for author in data_dtm.columns:
    top = [word for (word, count) in top_dict[author]]
    for t in top:
        words.append(t)

Counter(words).most_common()[:30]

from sklearn.feature_extraction import text

# If more than half of the authors have it as a top word, exclude it from the list
add_stop_words = [word for word, count in Counter(words).most_common() if count > 50]
add_stop_words

# Add new stop words
stop_words = text.ENGLISH_STOP_WORDS.union(add_stop_words)

# Recreate document-term matrix
cv = CountVectorizer(stop_words=stop_words)
data_cv_stop = cv.fit_transform(df_corpus.abstract)
df_dtm_stop = pd.DataFrame(data_cv_stop.toarray(), columns=cv.get_feature_names())
df_dtm_stop.index = df_corpus.index
data_dtm_stop = df_dtm_stop.transpose()


wc_stop = WordCloud(
    stopwords=stop_words,
    background_color="white",
    colormap="Dark2",
    max_font_size=150,
    random_state=42,
)


plt.rcParams["figure.figsize"] = [16, 6]

wc_dtm_stop = data_dtm_stop[pdgroup_names]
wc_corpus_stop = df_corpus.loc[pdgroup_names]

# Create subplots for each author
for index, author in enumerate(wc_dtm_stop.columns):
    wc_stop.generate(wc_corpus_stop.abstract[author])

    plt.subplot(2, 3, index + 1)
    plt.imshow(wc_stop, interpolation="bilinear")
    plt.axis("off")
    plt.title(wc_dtm_stop.columns[index])

plt.show()

## Unique and total words
unique_list = []
for author in data_dtm.columns:
    uniques = data_dtm[author].to_numpy().nonzero()[0].size
    unique_list.append(uniques)

author_names = df_corpus.index

# Create a new dataframe that contains this unique word count
data_words = pd.DataFrame(
    list(zip(author_names, unique_list)), columns=["author", "unique_words"]
)
data_unique_sort = data_words.sort_values(by="unique_words")
data_unique_sort

# Find the total number of words that an author uses
total_list = []
for authors in data_dtm.columns:
    totals = sum(data_dtm[authors])
    total_list.append(totals)

# Add to the dataframe
data_words["total_words"] = total_list
data_words_pdgroup = data_words[data_words.author.str.contains("|".join(pdgroup_names))]


# Sort the dataframes
data_wpm_sort = data_words_pdgroup.sort_values(by="total_words")
data_wpm_sort

data_unique_sort_pdgroup = data_words_pdgroup.sort_values(by="unique_words")
data_unique_sort_pdgroup


import numpy as np

y_pos = np.arange(len(data_words_pdgroup))

plt.subplot(1, 2, 1)
plt.barh(y_pos, data_unique_sort_pdgroup.unique_words, align="center")
plt.yticks(y_pos, data_unique_sort_pdgroup.author)
plt.title("Number of Unique Words", fontsize=20)

plt.subplot(1, 2, 2)
plt.barh(y_pos, data_wpm_sort.total_words, align="center")
plt.yticks(y_pos, data_wpm_sort.author)
plt.title("Number of Total Words", fontsize=20)

plt.tight_layout()
plt.show()


## Fluids vs models
Counter(words).most_common()[:50]

data_fm_words = data_dtm.transpose()[
    ["flow", "fluid", "flows", "liquid", "model", "method", "simulations", "models"]
]
data_form = pd.concat(
    [
        data_fm_words.flow
        + data_fm_words.fluid
        + data_fm_words.flows
        + data_fm_words.liquid,
        data_fm_words.model
        + data_fm_words.method
        + data_fm_words.simulations
        + data_fm_words.models,
    ],
    axis=1,
)
data_form.columns = ["fluids", "models"]
data_form_pdgroup = data_form[data_form.index.str.contains("|".join(pdgroup_names))]


plt.rcParams["figure.figsize"] = [10, 8]

for i, author in enumerate(data_form_pdgroup.index[:30]):
    x = data_form_pdgroup.fluids.loc[author]
    y = data_form_pdgroup.models.loc[author]
    plt.scatter(x, y, color="blue")
    plt.text(x + 0.2, y, pdgroup_names[i], fontsize=10)

plt.title("Number of Words Used in the Abstract", fontsize=20)
plt.xlabel("Number of fluid related words", fontsize=15)
plt.ylabel("Number of model related words", fontsize=15)
plt.axis("scaled")
plt.show()


# Sentiment analysis
from textblob import TextBlob

TextBlob("bad").sentiment
TextBlob("not bad").sentiment
TextBlob("very bad").sentiment

pol = lambda x: TextBlob(x).sentiment.polarity
sub = lambda x: TextBlob(x).sentiment.subjectivity

df_corpus["polarity"] = df_corpus["abstract"].apply(pol)
df_corpus["subjectivity"] = df_corpus["abstract"].apply(sub)
df_corpus


import matplotlib.pyplot as plt

plt.rcParams["figure.figsize"] = [10, 8]

for index, author in enumerate(df_corpus.index[:10]):
    x = df_corpus.polarity.loc[author]
    y = df_corpus.subjectivity.loc[author]
    plt.scatter(x, y, color="blue")
    plt.text(x, y, df_corpus.index[index], fontsize=10)

plt.title("Sentiment Analysis", fontsize=20)
plt.xlabel("<-- Negative -------- Positive -->", fontsize=15)
plt.ylabel("<-- Factual ------ Opinionated -->", fontsize=15)

plt.show()

df_base.sort_values("author").iloc[3].abstract

### Topic modelling ###

from gensim import matutils, models
import scipy.sparse


# One of the required inputs is a term-document matrix
# tdm = data_dtm_stop.transpose()
tdm = data_dtm_stop.transpose()
tdm.head()

# We're going to put the term-document matrix into a new gensim format, from df --> sparse matrix --> gensim corpus
sparse_counts = scipy.sparse.csr_matrix(tdm)
corpus = matutils.Sparse2Corpus(sparse_counts)

# Gensim requires dictionary of the all terms and their respective location in the term-document matrix
id2word = dict((v, k) for k, v in cv.vocabulary_.items())

# we need to specify two other parameters as well - the number of topics and the number of passes
# LDA for num_topics = 5

# lda = models.LdaModel(corpus=corpus, id2word=id2word, num_topics=5, passes=20)

import pickle

pickle.dump(lda, open("lda", "wb"))
lda = pickle.load(open("lda", "rb"))

lda.print_topics()

corpus_transformed = lda[corpus]
lda_list = list(zip([topic for topic in corpus_transformed], df_dtm_stop.index))
lda_df = pd.DataFrame(lda_list)
lda_df.columns = ["lda", "author"]

Goncalves_pos = lda_df.loc[lda_df["author"] == "Gabriel Goncalves"]
Goncalves = lda_df.iloc[Goncalves_pos.index[0]]
Goncalves.lda

Goncalves_pos2 = df_base.loc[df_base["author"] == "Gabriel Goncalves"]
Goncalves_base = df_base.iloc[Goncalves_pos2.index[0]]
Goncalves_base.abstract

### Text generation ###

all_text = df_corpus.groupby("abstract").apply(" ".join)  # .reset_index()
all_text2 = pd.Series.to_string(all_text)

from collections import defaultdict


def markov_chain(text):

    # Tokenize the text by word, though including punctuation
    words = text.split(" ")

    # Initialize a default dictionary to hold all of the words and next words
    m_dict = defaultdict(list)

    # Create a zipped list of all of the word pairs and put them in word: list of next words format
    for current_word, next_word in zip(words[0:-1], words[1:]):
        m_dict[current_word].append(next_word)

    # Convert the default dict back into a dictionary
    m_dict = dict(m_dict)
    return m_dict


# Create the dictionary
all_dict = markov_chain(all_text2)

import random

random.seed(57)


def generate_sentence(chain, count=50):

    # Capitalize the first word
    word1 = random.choice(list(chain.keys()))
    sentence = word1.capitalize()

    # Generate the second word from the value list. Set the new word as the first word. Repeat.
    for i in range(count - 1):
        word2 = random.choice(chain[word1])
        word1 = word2
        sentence += " " + word2

    # End it with a period
    sentence += "."
    return sentence


generate_sentence(all_dict)
