#!/usr/bin/env python

# name, title, code, link

from pathlib import Path

import pandas as pd
from loguru import logger
from bs4 import BeautifulSoup


def load_soup(filename):
    with open(filename) as f:
        soup = BeautifulSoup(f, "html5lib")
    return soup


def get_rows(soup):
    table = soup.find("table")
    rows = table.find_all("tr")  # tr stands for "table row"
    return rows


def create_contributor_records(row):
    contributor_records = []
    name = row.find("td").text.strip()  # td stands for "table data"
    talk_list = row.find_all("a")
    for talk in talk_list:
        code = talk.previous_element.strip()
        title = talk.text
        link = talk["href"]
        contributor_records.append(
            {"name": name, "title": title, "code": code, "link": link}
        )
    return contributor_records


records = []
files = list(Path("./raw_contributors").glob("*.html"))

for filename in files:
    logger.info(f"Processing {filename}")
    soup = load_soup(filename)
    rows = get_rows(soup)
    for row in rows:
        records.extend(create_contributor_records(row))


print(records)
df = pd.DataFrame.from_records(records)
print(df)
df.to_csv("contributors.csv")
