def write_text(filename, text):
    with open(filename, "w") as f:
        f.write(text)
