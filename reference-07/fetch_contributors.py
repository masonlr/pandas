#!/usr/bin/env python

import string

import requests
from loguru import logger

from util import write_text

LETTERS = string.ascii_uppercase
URL = "http://meetings.aps.org/Meeting/DFD19/PersonIndex/3772"


def fetch_presenter_text(initial):
    data = {
        "LastNameInitial": initial,
        "Keywords": "",
        "showid": "0",
        "Action": "Search",
    }
    response = requests.post(URL, data=data)
    return response.text


for letter in LETTERS:
    logger.info(f"Fetching {letter}")

    text = fetch_presenter_text(initial=letter)
    write_text(f"raw_contributors/{letter}.html", text)
