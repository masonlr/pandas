#!/usr/bin/env python

import asyncio

import pandas as pd
import requests
from aiohttp import ClientSession
from loguru import logger

from util import write_text

URL = "http://meetings.aps.org"

df = pd.read_csv("contributors.csv")


def create_codes_dict(df):
    codes_dict = {}
    for index, row in df.iterrows():
        code = row.code
        link = row.link
        codes_dict[code] = f"{URL}{link}"
    return codes_dict


async def fetch(semaphore, code, link, session):
    await semaphore.acquire()
    async with session.get(link) as response:
        text = await response.text()
        logger.info(link)
        write_text(f"raw_abstracts/{code}.html", text)
        semaphore.release()
        return code


codes_dict = create_codes_dict(df)
# print(codes_dict)


async def run():
    tasks = []
    semaphore = asyncio.Semaphore(400)
    async with ClientSession() as session:
        for code, link in codes_dict.items():
            task = asyncio.ensure_future(fetch(semaphore, code, link, session))
            tasks.append(task)
        output = await asyncio.gather(*tasks)


loop = asyncio.get_event_loop()
future = asyncio.ensure_future(run())
loop.run_until_complete(future)
