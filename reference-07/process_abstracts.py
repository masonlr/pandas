#!/usr/bin/env python

# name, title, code, link

from pathlib import Path

import pandas as pd
from loguru import logger
from bs4 import BeautifulSoup


def load_soup(filename):
    with open(filename) as f:
        soup = BeautifulSoup(f, "html5lib")
    return soup


def get_abstract(soup):
    abstract = soup.find("div", {"class": "largernormal"}).text.strip()
    return abstract


def get_contributors_citation(soup):
    contributors_citation = soup.find("meta", {"name": "citation_authors"})[
        "content"
    ].split(";")
    contributors_citation_record = {
        f"contributor_citation_{i}": contributor.strip()
        for i, contributor in enumerate(contributors_citation)
    }
    return contributors_citation_record


def get_contributors_and_institutions(soup):
    record = {}
    raw_contributor_list = soup.find_all("span", {"class": "largernormal"})
    for i, raw_contributor in enumerate(raw_contributor_list):
        contributor_and_institution = raw_contributor.find_all("span")
        if len(contributor_and_institution) == 2:
            record[f"contributor_{i}"] = contributor_and_institution[0].text.strip()
            institution = contributor_and_institution[1].text.strip()
            record[f"institution_{i}"] = institution[1:-1]  # remove parentheses
    return record


records = []
files = list(Path("./raw_abstracts").glob("*.html"))
for i, filename in enumerate(files):
    logger.info(f"Processing {filename}")
    code = filename.name.strip(".html")
    logger.debug(code)

    soup = load_soup(filename)
    abstract = get_abstract(soup)

    record = {"code": code, "abstract": abstract}

    contributors_citation_record = get_contributors_citation(soup)
    record.update(contributors_citation_record)

    contributors_and_institutions_record = get_contributors_and_institutions(soup)
    record.update(contributors_and_institutions_record)

    records.append(record)

df = pd.DataFrame.from_records(records)
print(df)
df.to_csv("abstracts.csv")
