#!/usr/bin/env python3

import networkx as nx
import matplotlib.pyplot as plt
from pathlib import Path


NODE_CENTER = "Matar, Omar"
NODE_RADIUS = 2
GRAPH_FILE = "graphs/contributors.graphml"

G = nx.read_graphml(GRAPH_FILE)
G_selected = nx.ego_graph(G, NODE_CENTER, radius=NODE_RADIUS)

pfigs = Path("figs")
pfigs.mkdir(parents=True, exist_ok=True)

plt.figure(figsize=(30, 20), dpi=200)
pos = nx.spring_layout(G_selected, k=0.4, seed=12345)
pos_labels = {}
y_off = 0.02
for k, v in pos.items():
    pos_labels[k] = (v[0], v[1] + y_off)
nx.draw_networkx_nodes(G_selected, pos, alpha=0.6, node_color="b")
nx.draw_networkx_edges(
    G_selected,
    pos,
    alpha=0.6,
    node_size=0,
    width=[d["weight"] for (u, v, d) in G_selected.edges(data=True)],
    edge_color="r",
)
nx.draw_networkx_labels(G_selected, pos_labels, fontsize=14)
plt.tight_layout()
plt.savefig(pfigs / f"{NODE_CENTER} - Graph.png")
