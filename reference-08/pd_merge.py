#!/usr/bin/env python

import pandas as pd 
import numpy as np
data = pd.Series(np.random.randn(9), index=[['a', 'a', 'a', 'b', 'b', 'c', 'c', 'd', 'd'], 
                                            [1,2,3,1,3,1,2,2,3]])

## Check out the indexing
data.index
## Partial indexing
data['b']
data['b':'c']
data.loc[['b', 'd']]
## Inner level
data.loc[:, 2]
## Rearranging data into a DataFrame
data.unstack()
data.unstack().stack()

## Using indices to sort data
frame = pd.DataFrame(np.arange(12).reshape((4, 3)),
                            index=[['a', 'a', 'b', 'b'], [1, 2, 1, 2]],
                            columns=[['Ohio', 'Ohio', 'Colorado'],
                            ['Green', 'Red', 'Green']])

frame.index.names = ['key1', 'key2']
frame.columns.names = ['state', 'color']
frame.swaplevel('key1', 'key2')
frame.sort_index(level=1)
frame.swaplevel(0, 1).sort_index(level=0)
frame.sum(level='key2')
frame.sum(level='color', axis=1)


## Merge or join operations combine datasets by linking rows using one or more keys. 
## These operations are central to relational databases (e.g., SQL-based). 
## The merge function in pandas is the main entry point for using these algorithms on your data.
 df1 = pd.DataFrame({'key': ['b', 'b', 'a', 'c', 'a', 'a', 'b'],
                            'data1': range(7)})
 df2 = pd.DataFrame({'key': ['a', 'b', 'd'],
                             'data2': range(3)})
df1
df2
pd.merge(df1, df2)
## Note that I didn’t specify which column to join on. 
# If that information is not specified, merge uses the overlapping column names as the keys. 
# It’s good practice to specify explicitly, though:

pd.merge(df1, df2, on='key')

##You may notice that the 'c' and 'd' values and associated data are missing from the result. By default merge does an 'inner' join; 
# the keys in the result are the intersec‐ tion, or the common set found in both tables. Other possible options are 'left', 'right', and 'outer'. 
# The outer join takes the union of the keys, combining the effect of applying both left and right joins:
pd.merge(df1, df2, on='key',how='outer')
pd.merge(df1, df2, on='key',how='left')
pd.merge(df1, df2, on='key',how='right')
pd.merge(df1, df2, on='key',how='inner')

contributors = pd.read_csv('contributors.csv')
abstracts = pd.read_csv('abstracts.csv')
merge=pd.merge(contributors,abstracts, on='code')
merge.sort_values(by=['code'])

## Merging on 2 keys
 left = pd.DataFrame({'key1': ['foo', 'foo', 'bar'],
                      'key2': ['one', 'two', 'one'],
                        'lval': [1, 2, 3]})
 right = pd.DataFrame({'key1': ['foo', 'foo', 'bar', 'bar'],
                       'key2': ['one', 'one', 'one', 'two'],
                        'rval': [4, 5, 6, 7]})
pd.merge(left, right, on=['key1', 'key2'], how='outer')

pd.merge(left, right, on='key1')
pd.merge(left, right, on='key1', suffixes=('_left', '_right'),how='inner')

 left2 = pd.DataFrame([[1., 2.], [3., 4.], [5., 6.]],
                      index=['a', 'c', 'e'],
                     columns=['Ohio', 'Nevada'])

 right2 = pd.DataFrame([[7., 8.], [9., 10.], [11., 12.], [13, 14]],
                       index=['b', 'c', 'd', 'e'],
                      columns=['Missouri', 'Alabama'])

## Merging on Index
pd.merge(left2, right2, how='outer', left_index=True, right_index=True)
## DataFrame has a convenient join instance for merging by index. It can also be used to combine together many 
# DataFrame objects having the same or similar indexes but non-overlapping columns. 
# In the prior example, we could have written:
left2.join(right2, how='outer')

## Concat
s1 = pd.Series([0, 1], index=['a', 'b'])
s2 = pd.Series([2, 3, 4], index=['c', 'd', 'e'])
s3 = pd.Series([5, 6], index=['f', 'g'])

pd.concat([s1, s2, s3])

##axis=1 is the columns
pd.concat([s1, s2, s3], axis=1)

s4 = pd.concat([s1, s3])

pd.concat([s1, s4], axis=1)
##get only intersection
pd.concat([s1, s4], axis=1, join='inner')
##You can even specify the axes to be used on the other axes with join_axes:
pd.concat([s1, s4], axis=1, join_axes=[['a', 'c', 'b', 'e']])
## Use keys to identify
result = pd.concat([s1, s1, s3], keys=['one', 'two', 'three'])