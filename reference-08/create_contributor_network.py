#!/usr/bin/env python3

import pandas as pd
import networkx as nx
from itertools import combinations, chain
from collections import Counter
from pathlib import Path


def pairs_from_row(row):
    return list(combinations(sorted(pd.unique(row.dropna().values)), 2))


def pairs_from_columns(columns):
    series_of_pairs = columns.apply(pairs_from_row, axis=1)
    return list(chain.from_iterable(series_of_pairs.values))


def weighted_graph_from_pairs(pairs):
    G = nx.Graph()
    G.add_edges_from(pairs)
    c = Counter(pairs)
    for u, v, d in G.edges(data=True):
        d["weight"] = c[tuple(sorted([u, v]))]
    return G


p = Path(__file__).parents[1] / Path("reference-07") / Path("abstracts.csv")
df = pd.read_csv(p)

pgraphs = Path("graphs")
pgraphs.mkdir(parents=True, exist_ok=True)

contributor_columns = df.filter(like="contributor_citation")
contributor_pairs = pairs_from_columns(contributor_columns)
G = weighted_graph_from_pairs(contributor_pairs)
nx.write_graphml(G, pgraphs / "contributors.graphml")

institution_columns = df.filter(like="institution")
institution_pairs = pairs_from_columns(institution_columns)
H = weighted_graph_from_pairs(institution_pairs)
nx.write_graphml(H, pgraphs / "institutions.graphml")
