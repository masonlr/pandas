import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

#Handling missing data
string_data= pd.Series(['Aadvark','artichoke',np.nan,'avacado'])
string_data
string_data.isnull()

#Filtering missing data
from numpy import nan as NA
data = pd.Series([1, NA, 3.5, NA, 7])
data
data.dropna()

data = pd.DataFrame([[1., 6.5, 3.], [1., NA, NA],[NA, NA, NA], [NA, 6.5, 3.]])
data
cleaned = data.dropna()
cleaned

data.dropna(how='all')

data[4] = NA
data
data.dropna(axis=1, how='all')

df = pd.DataFrame(np.random.randn(7, 3))
df
df.iloc[:4,1]= NA
df.iloc[:2,2]= NA
df

#filling in missing data
df.fillna(0)
df.fillna ({1:0.5, 2:1})

prop = pd.read_csv("https://raw.githubusercontent.com/dataoptimal/videos/master/cleaning%20messy%20data%20with%20pandas/property%20data.csv")
prop

prop['NUM_BEDROOMS'].isnull()
missing_values= ["na", "--"]
prop= pd.read_csv("https://raw.githubusercontent.com/dataoptimal/videos/master/cleaning%20messy%20data%20with%20pandas/property%20data.csv", na_values=missing_values)
prop
prop['NUM_BEDROOMS'].isnull()

cnt=0
for row in prop['OWN_OCCUPIED']:
    try:
        int(row)
        prop.loc[cnt,'OWN_OCCUPIED']=np.nan
    except ValueError:
        pass
        cnt+=1

prop

#Binning
titanic = pd.read_csv(
    "https://raw.githubusercontent.com/mwaskom/seaborn-data/master/titanic.csv"
)
titanic.head()
titanic
titanic.columns
titanic.isnull().sum()

newdata= titanic.dropna()
newdata
newdata.isnull().sum()

ages= newdata['age']
bins= [0,18,25,35,60,100]
people= pd.cut(ages,bins)
people

pd.value_counts(people)

group_names = ['Kids','Youth', 'YoungAdult', 'MiddleAged', 'Senior']
people_categ= pd.cut(ages, bins, labels=group_names)
people_categ

df= pd.value_counts(people_categ)
df.plot.bar()

#Mapping of data
data = pd.DataFrame({'food': ['bacon', 'pulled pork', 'bacon',
                              'Pastrami', 'corned beef', 'Bacon',
                              'pastrami', 'honey ham', 'nova lox'],
                     'ounces': [4, 3, 12, 6, 7.5, 8, 3, 5, 6]})

data

meat_to_animal = {
  'bacon': 'pig',
  'pulled pork': 'pig',
  'pastrami': 'cow',
  'corned beef': 'cow',
  'honey ham': 'pig',
  'nova lox': 'salmon'
}

lowercased= data['food'].str.lower()
lowercased

data['animal']= lowercased.map(meat_to_animal)
data

#Detecting Outliers
ages.describe()
newdata.columns
newdata['fare'].describe()
newdata['fare'].isnull().sum()
ages[np.abs(ages<10)].count()

#Random Sampling
choices = pd.Series([5, 7, -1, 6, 4])
draws= choices.sample(n=4)
draws

choices= ages.sample(n=5)
choices

#String Manupilation

val= 'a,b,  guide'
val.split(',')

pieces = [x.strip() for x in val.split(',')]
pieces

first, second, third= pieces
first + ':'+ second + ':' + third

val.count(',')
val.upper()

#Regular Expressions
import re

text = "USA    UK\t UAE  \tUganda"
re.split('\s+',text)

regex = re.compile('\s+')
regex.split(text)
regex.findall(text)

text1 = """Dave dave@google.com
Steve steve@gmail.com
Rob rob@gmail.com
Ryan ryan@yahoo.com
"""
text1

pattern = r'[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}'
regex = re.compile(pattern, flags=re.IGNORECASE)
regex.findall(text1)
print(regex.sub('REDACTED', text1))

pattern = r'([A-Z0-9._%+-]+)@([A-Z0-9.-]+)\.([A-Z]{2,4})'
regex = re.compile(pattern, flags=re.IGNORECASE)
regex.findall(text1)
m = regex.match('wesm@bright.com')
m.groups()

regex.sub(r'Username: \1, Domain: \2, Suffix: \3', text1)
