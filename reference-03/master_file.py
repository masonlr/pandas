#!/usr/bin/env python

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from numpy.random import randn


## Basic data plotting
data = np.arange(10)

plt.plot(data)
## Generating subplots
fig = plt.figure()
ax1 = fig.add_subplot(2, 2, 1)
ax2 = fig.add_subplot(2, 2, 2)
ax3 = fig.add_subplot(2, 2, 3)
ax4 = fig.add_subplot(2, 2, 4)

ax1.plot(np.random.randn(50).cumsum(), 'k--')
ax2.hist(np.random.randn(100), bins=20, color='k', alpha=0.3)
ax3.scatter(np.arange(30), np.arange(30) + 3 * np.random.randn(30))
## How can you run these two commands separately?
#---------------------------------------------#
###%Colors, Markers, and Line Styles

#ax.plot(x, y, 'g--')
#ax.plot(x, y, linestyle='--', color='g')

#Figure 9-6
from numpy.random import randn

plt.plot(randn(30).cumsum(), 'ko--')
#plt.plot(randn(30).cumsum(),linestyle=':', color='g')

plt.plot(randn(30).cumsum(), color='k', linestyle='dashed', marker='o')

#Figure 9-7, For line plots, you will notice that subsequent points are linearly interpolated by
#default. This can be altered with the drawstyle option

fig=plt.figure()
data = np.random.randn(30).cumsum()
plt.plot(data, 'k--', label='Default')

plt.plot(data, 'k-', drawstyle='steps-post', label='steps-post')

plt.legend(loc='best')

#ax.legend?

#plt.legend(loc='southeast')

#You must call plt.legend (or ax.legend, if you have a reference to
#the axes) to create the legend, whether or not you passed the label
#options when plotting the data.


#Figure 9-8

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(np.random.randn(1000).cumsum())

ticks = ax.set_xticks([0, 250, 500, 750, 1000])

labels = ax.set_xticklabels(['one', 'two', 'three', 'four', 'five'],
        rotation=30, fontsize='small')

ax.set_title('My first matplotlib plot')
ax.set_xlabel('Stages')

#or
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(np.random.randn(1000).cumsum())

props = {
'title': 'My first matplotlib plot',
'xlabel': 'Stages'
}
ax.set(**props)

#Figure 9-9

fig = plt.figure(); ax = fig.add_subplot(1, 1, 1)

ax.plot(randn(1000).cumsum(), 'k', label='one')
ax.plot(randn(1000).cumsum(), 'k--', label='two')
ax.plot(randn(1000).cumsum(), 'k.', label='three')

ax.legend(loc='best')

ax.legend?

#Figure 9-12

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
rect = plt.Rectangle((0.2, 0.75), 0.4, 0.15, color='k', alpha=0.3)
circ = plt.Circle((0.7, 0.2), 0.15, color='b', alpha=0.3)
pgon = plt.Polygon([[0.15, 0.15], [0.35, 0.4], [0.2, 0.6]],
color='g', alpha=0.5)
ax.add_patch(rect)
ax.add_patch(circ)
ax.add_patch(pgon)

#plt.savefig('figpath.png')
#plt.savefig('figpath.png', dpi=400, bbox_inches='tight')



## Pandas and plotting
## Figure 9-16/9-17
df = pd.DataFrame(np.random.rand(6, 4),index=['one', 'two', 'three', 'four', 'five', 'six'],columns=pd.Index(['A', 'B', 'C', 'D'], name='Zinelis'))
df.plot.bar()
df.plot.barh(stacked=True, alpha=0.5)

## Figure 9-18
tips = pd.read_csv('tips.csv')

tips

party_counts = pd.crosstab(tips['day'], tips['size'])

party_counts

party_counts = party_counts.loc[:, 2:5]
party_pcts = party_counts.div(party_counts.sum(1), axis=0)
party_pcts
party_pcts.plot.bar()

## Figure 9-19 Tipping percentage
tips['tip_pct'] = tips['tip'] / (tips['total_bill'] - tips['tip'])

tips.head()
sns.barplot(x='tip_pct', y='day', data=tips, orient='h')

## Seaborn automatically gives you mean value and 95% error bar.

## Figure 9-20 Splitting categorically using hue
sns.barplot(x='tip_pct', y='day', hue='time', data=tips, orient='h')

## Figure 9-21/9-22
## Histograms and density plots
tips['tip_pct'].plot.hist(bins=50)
tips['tip_pct'].plot.density()

## Figure 9-23
## Create Gaussian distributions with numpy and plot with seaborn
## Dist plot can plot histograms and density estimates simulataneously
comp1 = np.random.normal(0, 1, size=200)
comp2 = np.random.normal(10, 2, size=200)
values = pd.Series(np.concatenate([comp1, comp2]))
sns.distplot(values, bins=100, color='k')

## Figure 9-24
## Scatter or Points Plots with Seaborn
## regplot makes a scatter plot and fits linear regression line 
macro = pd.read_csv('macrodata.csv')
data = macro[['cpi', 'm1', 'tbilrate', 'unemp']]
trans_data = np.log(data).diff().dropna()
sns.regplot('m1', 'unemp', data=trans_data)
plt.title('Changes in log %s versus log %s' % ('m1', 'unemp'))



## Practical example
blue= pd.read_csv('blue.csv')
blue.head()
blue.columns
ax=plt.plot('Time', 'ptz(NORTH)', data=blue)
# plt.autoscale(ax, axis='both', tight=True)
# plt.show(ax)




fig, [[ax1, ax2], [ax3, ax4]] = plt.subplots(nrows=2, ncols=2)
ax1.plot('Time','Max(div(V))', data=blue)
ax1.ticklabel_format(ax=ax, style='sci',axis='both')
ax2.plot('Time','Kinetic Energy', data=blue)
ax3.plot('Time','MAX_VELOCITY_MAGNITUDE', data=blue)
ax4.plot('Time','ptz(SOUTH)', data=blue)
ax4.plot('Time','ptz(NORTH)', data=blue)
for ax in ['ax1', 'ax2', 'ax3', 'ax4']:
    plt.ticklabel_format(ax=ax, style='sci',axis='both')
plt.show(fig)

blue.columns

fig, [[ax1, ax2], [ax3, ax4]] = plt.subplots(nrows=2, ncols=2)
blue.plot('Time','Max(div(V))', ax=ax1)
blue.plot('Time','Kinetic Energy', ax=ax2)
blue.plot('Time','MAX_VELOCITY_MAGNITUDE', ax=ax3)
for cols in [ 'MIN_X_VELOCITY',
       'MIN_Y_VELOCITY', 'MIN_Z_VELOCITY', 'MIN_VELOCITY_MAGNITUDE',
       'MAX_X_VELOCITY', 'MAX_Y_VELOCITY', 'MAX_Z_VELOCITY',
       'MAX_VELOCITY_MAGNITUDE']:
       blue.plot('Time', cols, ax=ax4)

## Figure size
fig, [[ax1, ax2], [ax3, ax4]] = plt.subplots(nrows=2, ncols=2, figsize=(20, 15))
blue.plot('Time','Max(div(V))', ax=ax1)
blue.plot('Time','Kinetic Energy', ax=ax2, logy=True, logx=True,)
blue.plot('Time','MAX_VELOCITY_MAGNITUDE', ax=ax3)
for cols in [ 'MIN_X_VELOCITY',
       'MIN_Y_VELOCITY', 'MIN_Z_VELOCITY', 'MIN_VELOCITY_MAGNITUDE',
       'MAX_X_VELOCITY', 'MAX_Y_VELOCITY', 'MAX_Z_VELOCITY',
       'MAX_VELOCITY_MAGNITUDE']:
       blue.plot('Time', cols, ax=ax4)
       ax4.legend(ncol=3 )
fig.savefig('fig.png')