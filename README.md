# Pandas

* [reference-01](./reference-01): Introductory session (`Series`, `DataFrame`, `Index`)
* [reference-02](./reference-02): Introductory session (`reindex()`, `groupby()`)
* [reference-03](./reference-03): Data visualisation (`plot()`)
* [reference-04](./reference-04): Data Cleaning (`dropna()`,`findall()`)
* [reference-05](./reference-05): Introduction to Modelling (`OLS`,`AutoReg`,`LogisticRegression`,`LinearRegression`)
* [reference-06](./reference-06): Data Analysis Examples (`apply`,`transform`,`stack`,`pivot_table`)
* [reference-07](./reference-07): Web scraping (`requests`, `BeautifulSoup`)
* [reference-09](./reference-09): Natural Language Processing (`nltk`, `gensim`, `wordcloud`)
