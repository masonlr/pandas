# %% General inputs and packages
import pandas as pd
import numpy as np

import statsmodels.api as sm
import statsmodels.formula.api as smf


def dnorm(mean, variance, size=1):
    """Returns a random number (or list of numbers) obeying a normal 
       distribution"""
    if isinstance(size, int):
        size = (size,)
    return mean + np.sqrt(variance) * np.random.randn(*size)


# This seems to be outdated; the same could be acomplished with np.random.normal

#%% Generate some synthetic data

# For reproducibility
np.random.seed(12345)

# Number of points being generated
N = 100

# N vectors of 3 coordinates chosen randomly
X = np.c_[dnorm(0, 0.4, size=N), dnorm(0, 0.6, size=N), dnorm(0, 0.2, size=N)]

# A vector of random noise
eps = dnorm(0, 0.1, size=N)

import matplotlib.pyplot as plt

plt.figure()
plt.hist(X)

plt.figure()
plt.hist(eps)

# The true values of the coefficients
beta = [0.1, 0.3, 0.5]

# Generates the synthetic output vector y
y = np.dot(X, beta) + eps

X[:5]
y[:5]

#%% First way to specify the model

# This is if you want to include an intercept
X_model = sm.add_constant(X)

X_model[:5]

model = sm.OLS(y, X)  # use X_model if you want the intercept!

results = model.fit()

results.params

print(results.summary())

#%% Another way to specify the same model

data = pd.DataFrame(X, columns=["col0", "col1", "col2"])

data["y"] = y

data[:5]

results = smf.ols("y ~ col0 + col1 + col2", data=data).fit()
# To remove intercept:
# results = smf.ols("y ~ col0 + col1 + col2 - 1", data=data).fit()

results.params

results.tvalues

results.predict(data[:5])

#%% Time series analysis

init_x = 4
import random

# Generate time series for an autoregressive process
values = [init_x, init_x]
N = 1000
b0 = 0.8
b1 = -0.4
noise = dnorm(0, 0.1, N)

for i in range(N):
    new_x = values[-1] * b0 + values[-2] * b1 + noise[i]
    values.append(new_x)

# Plotting the data that was generated
import matplotlib.pyplot as plt

plt.plot(values)

MAXLAGS = 5

# AR has been deprecated!
# model = sm.tsa.AR(values)
# results = model.fit(MAXLAGS)

model = sm.tsa.AutoReg(values, MAXLAGS)
results = model.fit()
results.params

# Comparing the data to the predictions (elapsed and forecasting)
N_back = 30
N_forwards = 2
results.plot_predict(start=N + 2 - N_back, end=N + 1 + N_forwards)
plt.gca().plot(values[-N_back:], "o-")

#%% We can plot the values of the partial autocorrelation estimate to help
#   define the lags that should be included in the model!
partial_auto_correlation = sm.tsa.stattools.pacf(values, nlags=40)
plt.bar(range(len(partial_auto_correlation)), partial_auto_correlation)
# We see that the correlation is high for lags 0 (trivial), 1 and 2, as expected!

# %%
