#%%
import pandas as pd
import numpy as np

# Let's explore pandas and sklearn with a practical example of
# multiphase pipe flow data! This table of experimental results
# has been extracted from a pdf through OCR, so the data is quite
# messy... But let's try to use what we've seen so far to clean it up
# and do some simple modelling.

# Owen, 1986. An experimental and theoretical analysis of equilibrium
# annular flows. PhD thesis.

#%% Importing the database
df = pd.read_csv("owen1986.csv")
df.head()

#%% Converting the values to numbers

# Since there were wrong characters in some columns, they weren't automatically
# converted to numbers!

column_list = [
    "pressure",
    "liquid_mass_flux",
    "liquid_mass_flux_uncert",
    "gas_mass_flux",
    "gas_mass_flux_uncert",
    "pressure_gradient",
    "pressure_gradient_uncert",
    "temperature",
]
# errors=coerce makes it so that the values that can't be converted are
# replaced by NaN
df[column_list] = df[column_list].apply(pd.to_numeric, errors="coerce")

#%% Getting rid of innapropriate or unused data

# The data conversion from pdf misformatted the columns such that we have
# three extra columns at the end. Let's get rid of those.
df.drop(df.columns[[-1, -2, -3]], axis=1, inplace=True)
# We should've replaced all improper values by NaN by now, so let's go
# ahead and get rid of the rows containing NaN's
df.dropna(inplace=True)

# To simplify our visualization, let's get rid of some of the columns
# that won't be used here
df.drop(
    [
        "run",
        "liquid_mass_flux_uncert",
        "gas_mass_flux_uncert",
        "pressure_gradient_uncert",
        "liquid_film_mass_flux",
        "liquid_film_mass_flux_uncert",
    ],
    axis=1,
    inplace=True,
)

#%% Checking quality of flow regime data

# Let's now look at the column with the flow regimes
df.regime.unique()
# Since there's just a few errors, let's correct those manually
df["regime"] = df["regime"].replace(["AMular", "Annular-"], "Annular")
df["regime"] = df["regime"].replace(["Annular/wl5py", "Annular/wtspy"], "Annular/Wispy")

#%% Defining the parameters (nondimensional numbers)

gas_density = 1.2
liquid_density = 1000.0
gas_dynamic_viscosity = 1.8e-5
liquid_dynamic_viscosity = 1e-3
diameter = 32e-3
area = 0.25 * np.pi * diameter ** 2

# There's several ways to nondimensionalize, but let's use a naive approach here
liquid_velocity = df.liquid_mass_flux / (liquid_density * area)
gas_velocity = df.gas_mass_flux / (gas_density * area)

df["liquid_reynolds"] = (
    liquid_density * liquid_velocity * diameter / liquid_dynamic_viscosity
)
df["gas_reynolds"] = gas_density * gas_velocity * diameter / gas_dynamic_viscosity
df["pressure_gradient_multiplier"] = df.pressure_gradient / (
    liquid_density * liquid_velocity ** 2
)
# If we had different fluids, diameters etc, we could have other columns with
# Weber, Froude etc...

#%% Preliminary visualisation

# Let's use what we've learned before to visualize the data
import seaborn as sns

# First, plotting the regime map
sns.lmplot("liquid_reynolds", "gas_reynolds", data=df, hue="regime", fit_reg=False)

# Oops, maybe working with the logarithms will make things more manageable...
df["log_liquid_reynolds"] = np.log(df.liquid_reynolds)
df["log_gas_reynolds"] = np.log(df.gas_reynolds)
df["log_pressure_gradient_multiplier"] = np.log(df.pressure_gradient_multiplier)

sns.lmplot(
    "log_liquid_reynolds", "log_gas_reynolds", data=df, hue="regime", fit_reg=False
)

# For the pressure gradient, let's focus on only one regime for now
df_annular = df.loc[df["regime"] == "Annular"]
sns.scatterplot(
    "log_liquid_reynolds",
    "log_pressure_gradient_multiplier",
    data=df_annular,
    hue="log_gas_reynolds",
)

#%% Modelling

# Now, we'll try to develop *very simple* models to try to predict our data of
# flow regime and pressure gradient.
# Since the first one is limited to a set of categories, we will be dealing
# with a *classification* problem, while for the second one, we'll be dealing
# with a *regression* model.

from sklearn.linear_model import LogisticRegression, LinearRegression

# Different functions to measure quality of the models
from sklearn.metrics import accuracy_score, r2_score

# Let's stick  to one regime for now:
df[["is_annular"]] = df[["regime"]] == "Annular"

#%% Classification task: given the superficial velocity based Reynolds
#   numbers, try to determine whether we have annular flow.
X_class = df[["log_liquid_reynolds", "log_gas_reynolds"]]
Y_class = df[["is_annular"]]

# One of the most basic models is a logistic regression
# (the name is a bit misleading, in practice it's used for classification!)
regime_model = LogisticRegression()
regime_model.fit(X_class, Y_class)

# These are the coefficients that were obtained with the fit:
regime_model.coef_
regime_model.intercept_

# We could now pass a new input for our model and we'll get a prediction of
# Trues and Falses!
Y_class_predicted = regime_model.predict(X_class)
# We should be testing it with a different dataset, but for now let's
# just test whether the fit is reasonable:
accuracy_score(Y_class.values, Y_class_predicted)
# Our logistic regression achieves 92% accuracy, not terrible for a first try

# The model actually tries to predict a value in the range [0, 1] and then
# discretizes this output!
# These are the raw values being predicted:
regime_model.predict_proba(X_class)

#%% Regression task: let's get only the points in the annular regime and try
#   to predict the pressure gradient based on the same variables
X_reg = X_class[df.is_annular]
Y_reg = df.log_pressure_gradient_multiplier[df.is_annular]

# Similar as before:
pressure_model = LinearRegression()
pressure_model.fit(X_reg, Y_reg)

pressure_model.coef_
pressure_model.intercept_

Y_reg_predicted = pressure_model.predict(X_reg)
r2_score(Y_reg.values, Y_reg_predicted)
# 0.93 is not great, but once again, this is a very simple
# model/parametrization

# We can examine the values vs. the predictions:
g = sns.scatterplot(Y_reg.values, Y_reg_predicted)
x0, x1 = g.get_xlim()
y0, y1 = g.get_ylim()
lims = [max(x0, y0), min(x1, y1)]
g.plot(lims, lims, "k")

# Final comment, we might want to save our calibrated model in a file
# to use later! We can use pickle or joblib (the latter is recommended
# for sklearn)

# import pickle
# pickle.dump(pressure_model, open("pressure_model.p", "wb"))
# pressure_model = pickle.load(open("pressure_model.p", "rb"))

# from joblib import dump, load
# dump(pressure_model, 'pressure_model.joblib')
# pressure_model = load('pressure_model.joblib')

#%%
